import asyncio
import os
import sys
import tempfile
import webbrowser
from dataclasses import dataclass

import aiofiles
import frontmatter
import jinja2
import marko
from marko.ext.gfm import GFM

import quart
from quart import Quart, render_template_string, request, redirect


Available = {}

try:
    import commonmark as commonmark
    Available['commonmark'] = True
except ImportError:
    Available['commonmark'] = False

from marko import Markdown as MarkoMarkdown


J = os.path.join

app = Quart(__name__)
SrcPath = os.path.dirname(__file__)

@app.template_filter(name='escapejs')
def escapejs(value):
    _js_escapes = {
        '\\': '\\u005C',
        '\'': '\\u0027',
        '"': '\\u0022',
        '>': '\\u003E',
        '<': '\\u003C',
        '&': '\\u0026',
        '=': '\\u003D',
        '-': '\\u002D',
        ';': '\\u003B',
        u'\u2028': '\\u2028',
        u'\u2029': '\\u2029'
    }
    # Escape every ASCII character with a value less than 32.
    _js_escapes.update(('%c' % z, '\\u%04X' % z) for z in range(32))
    return jinja2.Markup("".join(_js_escapes.get(l, l) for l in value))


def findDir(dirname):
    def tryDir(_dirname):
        return os.path.isdir(_dirname)

    guess = ['.', SrcPath]
    while guess:
        _dir = guess.pop(0)
        if tryDir(J(_dir, dirname)):
            print("Found templates dir:", J(_dir, dirname), file=sys.stderr)
            return J(_dir, dirname)
        for x in os.listdir(_dir):
            if os.path.isdir(x):
                guess.append(x)
    raise Exception("Missing templates!")


TEMPLATE_DIR = findDir('templates')
IMAGES_DIR = J('imgs')
DATA_DIR = os.environ.get('DATA', 'data')


env = jinja2.Environment(
        enable_async=True,
        loader=jinja2.PackageLoader('app', TEMPLATE_DIR),
        autoescape=jinja2.select_autoescape(['html', 'xml'])
)
env.filters['escapejs'] = escapejs


async def run_in_executor(f, *args, **kw):
    loop = asyncio.get_running_loop()
    return await loop.run_in_executor(None, f, *args, **kw)


def url_for(*args, **kw):
    try:
        return quart.url_for(*args, **kw)
    except:
        return './%s.html' % kw.get('pagename', 'no-name')


class Path:
    def __init__(self, path: str, ext: str = ''):
        self.ext = ext
        self.path = path

    async def ReadFile(self, filename) -> bytes:
        fullname = J(self.path, filename + self.ext)
        async with aiofiles.open(fullname, mode='rb')  as f:
            return await f.read()

    async def ListAll(self) -> [str]:
        l = -len(self.ext)
        return [x.name[:l] for x in await run_in_executor(os.scandir, DATA_DIR)
                if x.name.lower().endswith(self.ext)]

    async def WriteFile(self, filename: str, content: str):
        fullname = J(self.path, filename + self.ext)
        async with aiofiles.open(fullname, mode='w',
                                 encoding='utf8') as f:
            await f.write(content)

    async def DeleteFile(self, filename: str):
        fullname = J(self.path, filename + self.ext)
        backup_name = fullname + '.old'
        def rename():
            if os.path.exists(backup_name):
                os.unlink(backup_name)
            os.rename(fullname, backup_name)
        return await run_in_executor(rename)


class Page:
    path = Path(DATA_DIR, '.md')

    def __init__(self, name, content, isNew, metadata=None):
        if metadata is None:
            metadata = {}
        self._isNew = isNew
        self.name = name
        self.content = content
        self.metadata = metadata

    @classmethod
    async def From(cls, pagename):
        src = await cls.path.ReadFile(pagename)
        metadata, content = frontmatter.parse(src)
        return cls(pagename, content, False, metadata)

    @classmethod
    async def New(cls, pagename):
        return cls(pagename, Wiki.EMPTY_PAGE_TEMPLATE, True)

    async def read(self):
        return self.content

    @property
    def content_bytes(self):
        return self.content.encode('utf8')

    def isNew(self):
        return self._isNew


ACTIONS = VIEW_ACTION, EDIT_ACTION = ('pages', 'edit')


def search(node, test_f):
    if test_f(node):
        return node
    if hasattr(node, 'children'):
        for child in node.children:
            x = search(child, test_f)
            if x:
                return x

def test_h1(node):
    return isinstance(node, marko.block.Heading) and node.level==1

def findAll(node, test_f):
    if test_f(node):
        yield node
    if hasattr(node, 'children'):
        for child in node.children:
            for x in findAll(child, test_f):
                yield x



class Wiki:
    EMPTY_PAGE_TEMPLATE = "# Marked in the browser\n\nRendered with **marked**."
    PROJECT_URL = 'https://bitbucket.org/tenuki/shinki'
    BULMA_CSS_DEFAULT = 'https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css'
    IMGS = {}
    TIMGS = {}
    Alive = False
    USE_JINJA = True
    ACTIONS = ACTIONS

    @property
    def BULMA_CSS(self):
        return os.environ.get('BULMA_CSS', self.BULMA_CSS_DEFAULT)

    def __init__(self, root='.'):
        self.root = root

    def getPages(self):
        return asyncio.create_task(self.a_getPages())

    async def a_getPages(self):
        return await Page.path.ListAll()

    def getPageUrl(self, pagename, **kw):
        return url_for('pages', pagename=pagename, **kw)

    @classmethod
    def SetAlive(cls, newState=True):
        cls.Alive = newState

    @classmethod
    def IsAlive(cls):
        return cls.Alive

    @classmethod
    def Init(cls):
        dirs = [IMAGES_DIR]
        while dirs:
            _dir = dirs.pop(0)
            for x in os.scandir(_dir):
                if not x.is_dir():
                    fullname = J(_dir, x.name)
                    if 'thumb' in fullname.lower():
                        cls.TIMGS[x.name] = fullname
                    else:
                        cls.IMGS[x.name] = fullname
                else:
                    print(f"Scanning {x.name}.. ", end='\r', file=sys.stderr)
                    dirs.append(J(_dir, x.name))
        print(file=sys.stderr)

    @classmethod
    async def GetImage(cls, name):
        fullname = cls.IMGS[name]
        print(f'{name} -> {fullname} ..', file=sys.stderr)
        async with aiofiles.open(fullname, mode='rb') as f:
            return await f.read()

    @classmethod
    async def SavePage(cls, pagename, newContent):
        return await Page.path.WriteFile(pagename, newContent)

    @classmethod
    async def RenderPage(cls, pagename, action=VIEW_ACTION):
        try:
            page = await Page.From(pagename)
        except FileNotFoundError:
            page = await Page.New(pagename)

        async with aiofiles.open(J(TEMPLATE_DIR, "index.html"), mode='r',
                                 encoding='utf8') as f:
            template = await f.read()

        context = dict(wiki=cls(), page=page, action=action,
                       actions=cls.ACTIONS,
                       VIEW_ACTION=VIEW_ACTION, EDIT_ACTION=EDIT_ACTION)
        # if not cls.IsAlive():
        #    context['marked'] = await run_in_executor(render_markdown, page.content_bytes)
        md = await md_service(page.content)
        context['marked'] = md.html
        h1 = search(md.ast, test_h1)
        if h1:
            title = ' '.join((x.children
                              for x in findAll(h1,
                               lambda n:isinstance(n, marko.inline.RawText))))
        else:
            title = 'no title'
        context['title'] =title
        render_f = cls._render_jinja2 if cls.USE_JINJA else cls._render_quart
        return await render_f(template, context)

    @classmethod
    async def DeletePage(cls, pagename, pagedata):
        if not (pagename in pagedata):
            raise Exception("Request ignored")
        return Page.path.DeleteFile(pagename)

    @classmethod
    async def _render_quart(cls, template, context):
        """renders jinja2 template in context using quart's functions"""
        return await render_template_string(template, **context)

    @classmethod
    async def _render_jinja2(cls, template, context):
        """renders jinja2 template in context using jinja2 library functions"""
        tpl = env.from_string(template)
        return await tpl.render_async(**context)


@dataclass
class MDWithRender:
    md: str
    ast: object=None
    html: str=''


@app.route("/")
async def root():
    return quart.redirect('/pages/')


@app.route("/imgs/<string:name>")
async def pics(name):
    if any(name.lower().endswith(x) for x in ('.png', '.jpg',)):
        contents = await Wiki.GetImage(name)
        return quart.Response(contents, headers={'Content-type': 'image/png'})
    
    async with aiofiles.open(os.path.join('imgs',name), mode='rb') as f:
         content = await f.read()
    return quart.Response(content, headers={'Content-type': 'text/css'})


@app.route('/shinki/md', methods=['POST'])
async def md_entrypoint():
    form = await request.form
    md = await md_service(form['text'])
    return quart.Response(md.html,
                          mimetype='application/octet-stream',
                          headers={'Content-type': 'application/octet-stream'})


async def md_service(source) -> MDWithRender:
    return await Marko().convert_to_html(source)


class MarkdownService:
    async def convert_to_html(self, source):
        raise NotImplementedError


class Marko(MarkdownService):
    def __init__(self):
        self.markdown = MarkoMarkdown()
        self.markdown.use(GFM)
        # self.markdown.use("footnote")
        # self.markdown.use("toc")

    async def convert_to_html(self, source)->MDWithRender:
        ast = self.markdown.parse(source)
        html = self.markdown.render(ast)
        return MDWithRender(md=source, ast=ast, html=html)


@app.route('/<string:action>/<path:pagename>')
@app.route('/<string:action>/', defaults={'pagename': 'index'})
async def pages(pagename, action=VIEW_ACTION):
    if action != EDIT_ACTION:
        action = VIEW_ACTION
    return await Wiki.RenderPage(pagename, action)


@app.route('/<string:action>/<path:pagename>', methods=['POST'])
@app.route('/<string:action>', methods=['POST'], defaults={'pagename': 'index'})
async def save_page(pagename, action=VIEW_ACTION):
    form = await request.form
    newdata = form['text']
    try:
        await Wiki.SavePage(pagename, newdata)
    except Exception as err:
        print(f"Error saving: {pagename} ..  {err}", file=sys.stderr)
        return "Cannot save document", 500, {'X-Error': str(err)}
    return await pages(pagename)


@app.route('/<string:action>/<path:pagename>', methods=['DELETE'])
async def delete_page(pagename, action=VIEW_ACTION):
    form = await request.form
    pagedata = form['text']
    try:
        await Wiki.DeletePage(pagename, pagedata)
    except Exception as err:
        print(f"Error deleting: {pagename} ..  {err}", file=sys.stderr)
        return "Cannot remove document", 500, {'X-Error': str(err)}
    outurl = url_for('pages', pagename='/', action=VIEW_ACTION)
    return redirect(outurl)


class ArgCmd:
    cmd = None
    nargs = None

    def __init__(self, cmd=None, nargs=None):
        if cmd:
            self.cmd = cmd
        if nargs:
            self.nargs = nargs
        self.found = None

    def proc(self, args):
        if None in (self.cmd, self.nargs):
            raise Exception("No cmd especified!")
        args = args[:]
        found = False
        found_args = []
        for idx, arg in enumerate(args):
            if arg.lower() in ('-' + self.cmd, '--' + self.cmd):
                for n in range(self.nargs):
                    found_args.append(args[idx + 1 + n])
                found = True
                break
        self.found = found
        if found:
            self.fargs = found_args
        return found


class RenderPage(ArgCmd):
    cmd = 'render-page'
    nargs = 1

    async def run(self):
        if not self.found:
            raise Exception("Cmd not checked, call proc first!")
        print(await Wiki.RenderPage(self.fargs[0].replace('.md', '')))


class RenderSite(ArgCmd):
    cmd = 'render-site'
    nargs = 0

    async def run(self):
        if not self.found:
            raise Exception("Cmd not checked, call proc first!")
        await self._run()

    async def _run(self):
        wiki = Wiki()
        temp = tempfile.mkdtemp()
        for pagename in await wiki.a_getPages():
            print(f'Exporting {pagename}..', file=sys.stderr)
            with open(J(temp, pagename + '.html'), 'wb') as f:
                f.write((await Wiki.RenderPage(pagename)).encode('utf8'))
        webbrowser.open_new(temp)


async def main():
    Wiki.Init()
    for arg in [RenderPage(), RenderSite()]:
        if arg.proc(sys.argv):
            await arg.run()
            return True


if __name__ == "__main__":
    if not asyncio.run(main()):
        Wiki.SetAlive()
        app.run(host=os.environ.get('HOST', '127.0.0.1'), port=os.environ.get('PORT', '5001'))
