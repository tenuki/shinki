# This is [Shinki](https://bitbucket.org/tenuki/shinki) a minimal markdown jinja-wiki..
#### .. with static generation capabilities

This a minimalistic wiki system made with python3. It handles [commonmark](https://commonmark.org/) as markup language for its contents and uses the powerful [Jinja](https://jinja.palletsprojects.com/en/2.11.x/) template system.

### Usage

1. create an environment (`virtualenv env`) and activate it: 
	  * windows: `env\scripts\activate` 
	  * unix: `. env/bin/activate`
2. install dependencies: `pip install -r requirements.txt`
3. start wiki: `python app.py`
4. and you are ready to connect to http://localhost:5000/ and start editing your content (remember to save created/edit pages!)

### Content

Source content is taken and kept from markdown files in the execution directory.

### Export content

If you want to export all pages as static content into a newly created directory:
1. `python app.py --render-site`

Instead if you want to export only one page:
1. `python app.py --render-page PageName`