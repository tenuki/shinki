FROM python:3-alpine
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . . 
VOLUME /data
ENV DATA=/data
ENV HOST=0.0.0.0
CMD [ "python", "app.py" ] 
