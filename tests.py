import random
import sys

import pytest

from app import app, Wiki, escapejs, VIEW_ACTION, url_for


@pytest.fixture(name='app')
def _test_app(tmpdir):
    app.config['DATABASE'] = str(tmpdir.join('blog.db'))
    #init_db()
    return app

# @pytest.mark.asyncio
# async def test_geturl(app):
#     client = app.test_client()
#     response = await client.get('/')
#     x = app.url_for('pages', pagename='/', action=VIEW_ACTION)
#     print(repr(x))
#     assert x=='/pages/'

@pytest.mark.asyncio
async def test_app(app):
    client = app.test_client()
    response = await client.get('/')
    assert response.status_code == 302
    assert response.headers['location']=='/pages/'


@pytest.mark.asyncio
async def test_app_pages_root(app):
    client = app.test_client()
    response = await client.get('/pages/')
    assert response.status_code == 200
    data = await response.get_data(raw=False)
    assert escapejs(Wiki.EMPTY_PAGE_TEMPLATE) in data

@pytest.mark.asyncio
async def test_app_pages_root_setpage(app):
    client = app.test_client()
    TEXT_SAMPLE = 'asdfasdfasdfasdf'
    PAGENAME = str(random.random()).replace('.', '')*2
    response = await client.post('/pages/'+PAGENAME, form={
                                 'text': TEXT_SAMPLE})
    data = await response.get_data(raw=False)
    assert response.status_code == 200

    # verify new content in page
    response = await client.get('/pages/'+PAGENAME)
    assert response.status_code == 200
    data = await response.get_data(raw=False)
    assert TEXT_SAMPLE in data

    # delete page
    response = await client.delete('/pages/'+PAGENAME, form={
                                   'text': 'Please delete %s, mr!'%PAGENAME})
    data = await response.get_data(raw=False)
    print('relete deturned:', data)
    assert response.status_code == 302


